# JavaSE基础

# 1. Java入门

## a. JDK17的安装

bin -----> java.exe javac.exe

docs ----> Api文档

jmods ----> 包模块

## b. cmd常见命令

查看编译工具和运行工具版本号 `javac -version`` java -version`

```shell
E: --切换到E盘
cd [目录]  --进入指定的目录
cd ..      --退回到上一级目录
cd /       --退回到根目录
dir        --显示当前目录下所有的内容
cls        --清空屏幕
```

## c. Java入门程序

编写Java程序的步骤

编写一个Java程序需要经过3个步骤：编写代码、编译代码、运行代码

- 编写代码：任何一个文本编辑器都可以写代码，如windows的记事本
- 编译代码：将人能看懂的`.java`文件转换为Java虚拟机能够执行的字节码文件
- 运行代码：将字节码文件交给`Java`虚拟机执行

```java
public class HelloWorld{
    public static void main(String[] args){
        System.out.println("Hello World!");
    }
}
```

## d. JDK的组成

JDK由JVM、核心类库、开发工具组成，如下所示：

![img](https://gitee.com/masonlisq/mason_pic/raw/master/blogs_pic/202307312100729.jpeg)

- JVM(Java Virtual Machine)：Java虚拟机，真正运行Java程序的地方。
- 核心类库：Java自己写好的程序，给程序员自己程序调用的。
- JRE(Java Runtime Environment): Java的运行环境。
- JDK(Java Development Kit): Java开发工具包。

```latex
- 什么是JVM?
答：JDK最核心的组成部分是JVM（Java Virtual Machine），它是Java虚拟
机，真正运行Java程序的地方。
- 什么是核心类库？
答：它是Java本身写好的一些程序，给程序员调用的。 Java程序员并不是凭空开
始写代码，是要基于核心类库提供的一些基础代码，进行编程。
- 什么是JRE?
答：JRE（Java Runtime Enviroment），意思是Java的运行环境；它是由JVM
和核心类库组成的；如果你不是开发人员，只需要在电脑上安装JRE就可以运行Java程
序。
- 什么是开发工具呢？
答：Java程序员写好源代码之后，需要编译成字节码，这里会提供一个编译工具叫
做javac.exe，编写好源代码之后，想要把class文件加载到内存中运行，这里需要用到
运行工具java.exe。
除了编译工具和运行工具，还有一些其他的反编译工具、文档工具等待...
```

JDK、JRE的关系用一句话总结就是：用JDK开发程序，交给JRE运行

## e. API文档docs目录下的index.html

在开发过程中，如果遇到疑难问题，除了可以在互联网中查询解决办法外，还可以在 Java API帮助文档（以下称为“API文档”）中查找答案。API文档是由Oracle公司提供的一整套文档资料，包括Java各种技术的详细资料和JDK中提供的各种类的帮助说明。API文档被视为Java开发人员必备的权威参考资料，类似于字典一样。在开发过程中，要养成查阅API文档的习惯，以在其中寻找答案和解决方案。

## f. Java的跨平台原理

Java程序的执行是依赖于Java虚拟机的。就是因为有了 Java虚拟机所以Java程序有一个重要的特性叫做跨平台性。 

什么是跨平台运行呢？ 

- 所谓跨平台指的是用Java语言开发的程序可以在多种操作系统上运行，常见的操作系统有Windows、Linux、MacOS系统。 
- 如果没有跨平台性，同一个应用程序，想要在多种操作系统上运行，需要针对 
- 各个操作系统单独开发应用。比如微信有Windows版本、MacOS版本、Android版本、IOS版本 

为什么**Java**程序可以跨平台呢？ 

- 跨平台性的原理是因为在不同版本的操作系统中安装有不同版本的**Java**虚拟 

机，Java程序的运行只依赖于Java虚拟机，和操作系统并没有直接关系。从而做 

到一处编译，处处运行。

# 2. Java基础语法

## a. 注释

- 什么是注释？

注释是解释说明程序的问题，方便自己和别人阅读代码

- 注释有哪几种？格式怎样？

```java
1.单行注释：
// 后面根解释文字
2.多行注释
/*
这里写注释文字
可以写多行
*/
3.文档注释
/**
这里写文档注释
也可以写多行，文档注释可以利用JDK的工具生成帮助文档
*/
```

![img](https://gitee.com/masonlisq/mason_pic/raw/master/blogs_pic/202307312100621.png)

## b. 变量

### ⅰ. 什么是变量？

变量是用来记录程序中的数据的。其本质上是内存中的一块区域，你可以把这块区域理解成一个小盒子。

### ⅱ. 命名

![img](https://gitee.com/masonlisq/mason_pic/raw/master/blogs_pic/202307312100994.png)

### ⅲ. 为什么要使用变量？

使用变量来记录数据，对于数据的管理更为灵活。

### ⅳ. 变量的注意事项

```java
1.变量定义在哪个{}范围内，就只在哪个大括号内有效。变量的有效范围称之为变量的作
用域
{
int a = 10;
System.out.println(a); //这是是对的
}
System.out.println(a); //这里会出错
2.在同一个作用域内，不能有两个同名的变量
{
int a = 10;
int a = 20; //这里会出错
}
3. 变量没有初始化，不能直接使用
int a; // 仅仅定义了变量，但是没有初始值
System.out.println(a); // 这里会出错
4.变量可以定义在同一行
如：int a=10, b=20; // a和b都是int类型
// int a,b; // 定义变量
// 一堆代码
int a = 10; // 赋值
int b = 20; // 赋值
```

### ⅴ. 小结

- 变量是用来记录程序中的数据的，可以把变量理解成内存中的小盒子，盒子里 放的东西就是变量记录的数据
- 变量的定义格式： `数据类型 变量名 = 初始值`；
- 变量记录的数据程序运行过程中是可以发生改变的：` 变量名 = 值`;

## c. 标识符

标识符规则：

- 标识符由字母（ A~Z 和 a~z ）、数字（0~9）、下划线（_）、美元符号$ 

以及部分Unicode字符集（各符号之间没有空格）组成。 

- 标识符的首字母以字母、下划线或美元符号开头，后面可以是任何字母、数 

字、美元符号或下划线，但不能以数字开头。 

- 标识符的命名不能是关键字、布尔值（true、false）和null。 
- 标识符区分大小写，没有长度限制。

## d. 关键字

### ⅰ. 什么是关键字？ 

关键字是java语言中有特殊含义的单词。比如用int表示整数，用double表示小 

数，等等！ 

### ⅱ. 关键字有哪些？

一共有51个关键字：

```java
abstract assert boolean break byte case catch char 
class const continue default do double else enum 
extends final finally float for goto if implements 
import instanceof int interface long native new 
package private protected public return strictfp 
short static super switch synchronized this throw 
throws transient try void volatile while _
```

《Java开发手册》

https://developer.aliyun.com/ebook/386/92065?spm=a2c6h.26392470.ebook-read.9.22bb52e0SP6yhU

（1）代码中的命名均不能以下划线或美元符号开始，也不能以下划线或美 

元符号结束。 

（2）所有编程相关的命名严禁使用拼音与英文混合的方式，更不允许直接 

使用中文的。 

（3）使用 UpperCamelCase 风格，每个单词首字母都大写，在Java中，类 

名、接口名、枚举名都使用该风格，该风格还被称为帕斯卡命名法。 

HelloWorld 

（4）方法名、参数名、成员变量、局部变量都统一使用 LowerCamelCase 

风格，第一个单词首字母小写，其余字母单词首字母都大写。 

## e. 数据类型

### ⅰ. 计算机的最小存储单位

计算机中最小的存储单位是字节（**Byte**），一个字节占**8**位（**bit**）

### ⅱ. 基本数据类型

![img](https://gitee.com/masonlisq/mason_pic/raw/master/blogs_pic/202307312100988.png)

示例：

```java
public class TypeDemo1 {
public static void main(String[] args) {
// 目标：掌握8种基本数据类型，用来定义变量。
// 1、整型
byte number = 98;
System.out.println(number);
short number2 = 9000;
int number3 = 12323232; // 默认
// 注意：随便写一个整型字面量，默认是int类型的，73642422442424虽
然没有超过long的范围，但是它超过了本身int的范围了。
// 如果希望随便写一个整型字面量是long类型的，需要在其后面加上L/l
long number4 = 73642422442424L;
// 2、浮点型
// 注意:
// 随便写一个小数字面量，默认当成double类型对待的，
// 如果希望这个小数是float类型的，需要在后面加上：F/f
float score1 = 99.5F;
double score2 = 99.8; // 默认定义方案。
// 3、字符型
char ch1 = 'a';
char ch2 = '中';
char ch3 = '国';
// 4、布尔型
boolean b1 = true;
boolean b2 = false;
// 引用数据类型：String.
// String代表的是字符串类型，定义的变量可以用来表示字符串。
String name = "黑马 KFM";
System.out.println(name);
}
}
```

## f. 数据类型转换

类型转换的形式总体分为2种，一种是自动类型转换（扩展原始转换），一种是强制类型转换（缩小原始转换）。

### ⅰ. 什么是自动类型转换

```java
答：自动类型转换指的是，数据范围小的变量可以直接赋值给数据范围大的变量
byte a = 12;
int b = a; // 这里就发生了自动类型转换(把byte类型转换int类型)
```

### ⅱ. 自动类型转换的原理是怎样的？

答：自动类型转换其本质就是在较小数据类型数据前面，补了若干个字节。

![img](https://gitee.com/masonlisq/mason_pic/raw/master/blogs_pic/202307312100292.jpeg)

转换顺序如下：

byte -> short -> int -> long -> float ->double

chat -------->int



示例：

```java
public class TypeConversionDemo1{
    public static void main(String[] args){
        byte a = 12;  //8位
        int b = a;    //32位
        System.out.println(a); //12
        System.out.println(b); //12

        int c = 100;	//32位
        double d = c;	//64位
        System.out.println(d);	//100

        char ch = 'a';	//16位
        int i = ch;
        System.out.println(i);	//97
    }
}
```

### ⅲ. 表达式的自动类型转换

多种数据类型参与运算，其结果以打的数据类型为准

byte,short,char 三种类型数据在和其他类型数据运算时，都会转换成int类型再运算

```java
public class TypeConversionDemo2{
    public static void main(String[] args){
        byte a = 10;
        int b = 20;
        long c = 30;
        long rs = a + b + c;
        System.out.println(rs);
 
        double rs2 = a + b +1.0;
        System.out.println(rs2);

        byte i = 10;
		short j = 30;
		int rs3 = i + j;
		System.out.println(rs3);

        short s = 1;
		s = s + 1; // short ==> int 去进行运算的 结果是 int 类型的
		System.out.println(s); // 报错
        
		// 分析下面的程序运行结果
		byte b = 1;
		short s = 2;
		short x = b + s; // byte + short = int
		System.out.println(x); // 报错
        
		short s = 1;
		s = 18 + 1;
		System.out.println(s); // 19
    }
}
```

### ⅳ. 强制类型转换

#### 1. 什么是强制类型转换

强行将范围大的数据，赋值给范围小的变量也是可以的，这里就需要用到强制类型转换。

```java
目标数据类型 变量名 = (目标数据类型)被转换的数据;
int a = 10;
byte b = (byte)a;
int a = 128;
byte b = (byte)a;
```

示例：

```java
public class TypeConversionDemo3 {
public static void main(String[] args) {
// 目标：掌握强制类型转换。
int a = 20;
byte b = (byte) a;
System.out.println(a);
System.out.println(b);
int i = 1500;
byte j = (byte) i;
System.out.println(j);
double d = 99.5;
int m = (int) d; // 强制类型转换
System.out.println(m); // 丢掉小数部分，保留整数部分
}
}
```

#### 2. 强制类型转换的原理

![img](https://gitee.com/masonlisq/mason_pic/raw/master/blogs_pic/202307312100585.jpeg)

### ⅴ. 字符在计算机的存储原理

字符通过ASCII编码表进行存储

| 字符 | 编码 |
| ---- | ---- |
| A    | 65   |
| Z    | 90   |
| a    | 97   |
| z    | 122  |

## g. 获取用户键盘输入的数据

```java
//【第1步】：在class类上导包：一般不需要我们自己做，ide工具会自动帮助我们导包
的。
import java.util.Scanner;
//【第2步】：得到一个用于键盘扫描器对象（照抄代码就行，固定格式）
// Scanner是键盘扫描器对象(你就把它理解成一个东西)，这个东西有录入的功
能
// sc是给这个东西取的名字
Scanner sc = new Scanner(System.in);
//【第3步】：开始调用sc的功能，来接收用户键盘输入的数据。
// sc这个东西有键盘录入整数的功能，这个功能的名字叫nextInt()
// .表示表示调用的意思
int age = sc.nextInt();
System.out.println("我的年龄是:"+age);
// sc这个东西还有键盘录入字符串的功能，这个功能的名字叫next
String name = sc.next();
System.out.println("我的姓名是:"+name);
```