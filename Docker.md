# Docker



# 1.镜像操作

![img](https://gitee.com/masonlisq/mason_pic/raw/master/blogs_pic/202307271439764.png)

查看docker版本 `docker -v`

查看docker 运行状态 `systemctl status docker`

重启`docker systemctl restart docker`（可以解决error responsefrom daemon）

- 查看所有镜像 `docker images`
- docker rmi 删除镜像
- docker push 推送镜像到服务器
- docker pull 从服务器拉取镜像
- docker save 保存镜像为压缩包
- docker load 加载压缩包为镜像

## 拉取、查看镜像

查看已安装镜像

```shell
docker images
```

根据以查看到的镜像名称，按需拉取镜像

```shell
docker pull nginx:latest
```

再次查看是否拉取到 `docker images`

## 保存、导入镜像

- 利用docker save将nginx镜像导出磁盘，然后再通过load加载回来

查看save命令语法 `docker save --help`

格式：`docker save -o [保存的目标文件名称][镜像名称]`

实例：`docker save -o nginx.tar nginx:latest`

删除本地nginx镜像：`docker rmi nginx:latest`

加载本地文件：`docker load -i nginx.tar`



## 小结

- `docker pull `命令拉取镜像
- `docker save` 命令将镜像保存为tar包
- `docker rmi `命令删除本地镜像
- `docker load` 重新加载镜像

# 2.容器操作

![img](https://gitee.com/masonlisq/mason_pic/raw/master/blogs_pic/202307271439608.png)

## 容器相关命令

容器保护三个状态：

- 运行：进程正常运行
- 暂停：进程暂停，CPU不再运行，并不释放内存
- 停止：进程终止，回收进程占用的内存、CPU等资源

容器的控制命令 + （名称/唯一标识符）：

- `docker run` : 创建并运行一个容器，处于运行状态
- `docker stop`：停止一个运行的容器
- `docker start`：让一个停止的容器再次运行
- `docker restart`：重新启动容器
- `docker rm`：删除一个容器
- `docker pause`：让一个运行的容器暂停
- `docker unpause`：让一个容器从暂停状态恢复运行

## 创建并运行容器

需求：创建并运行nginx

```shell
docker run --name name -p <宿主服务器端口>:<docker端口> -d <镜像名称>
```

运行nginx容器

```shell
docker run --name ng -p 8080:80 -d nginx:latest
```

![img](https://gitee.com/masonlisq/mason_pic/raw/master/blogs_pic/202307271439649.png)

查看运行的容器列表

```shell
docker ps
```

访问docker容器中的nginx

在浏览器输入docker容器宿主主机ip和nginx的映射端口

![img](https://gitee.com/masonlisq/mason_pic/raw/master/blogs_pic/202307271439655.png)

查看docker容器中的nginx访问日志

```shell
docker logs ng
```

![img](https://gitee.com/masonlisq/mason_pic/raw/master/blogs_pic/202307271439215.png)

持续查看docker容器中的nginx访问日志

```shell
docker logs -f ng
```

停止持续访问日志：`ctrl+c`

## 小结

`docker run` 命令的参数：

`--name`：指定容器名称

`-p`：指定端口映射

`-d`：让容器后台运行

查看容器日志的命令：`docker logs`

持续查看日志` docker logs -f`

查看容器状态：`docker ps`

查看所有容器，包括停止运行的 `docker ps -a`

# 3.数据卷

- 镜像中包含了应用程序及其所需的依赖、函数库、环境、配置等，还包括了应用程序的数据。如果在应用程序运行过程中修改了数据，**那么删除镜像后，数据也同时被删除了。**
- **原因：产生这种问题的原因是容器与数据（容器内的文件）耦合带来的后果。**
- **要解决这个问题，必须将数据与容器解耦，就必须用到数据卷。**

## 数据卷的定义

数据卷（volume）是一个虚拟目录，指向宿主机文件系统中的某个目录。![img](https://gitee.com/masonlisq/mason_pic/raw/master/blogs_pic/202307271439952.png)

一旦完成数据挂载，对容器的一切操作都会作用在数据卷对应的宿主机目录中了。这样，我们操作宿主机的/var/lib/docker/volumes/html目录，就相当于操作容器内的/usr/share/niginx/html目录了。

## 数据卷操作命令

基本语法：

```shell
docker volume [command]
```

docker volume命令是数据卷操作，根据命令跟随的command来确定下一步的操作。

- `create`创建一个volume
- `inspec`t显示一个或多个volume的信息
- `ls`列出所有的volume
- `prune`删除未使用的volume
- `rm`删除一个或多个指定的volume

## 创建和查看数据卷

创建数据卷

```shell
docker volume create html 
```

查看所有数据卷

```shell
docker volume ls
```

查看数据卷详细信息卷

```shell
docker volume inspect html
```

小结：

数据卷的作用：

- 将容器与数据分离，解耦，方便操作容器内的数据，保证数据安全

数据卷操作：

- `docker volume create`：创建数据卷
- `docker volume ls`：查看所有数据卷
- `docker volume inspect`：查看数据卷详细信息，包括关联的宿主机目录位置
- `docker volume rm`：删除指定数据卷
- `docker volume prume`：删除所有未使用的数据卷

## 挂载数据卷

我们在创建容器时，可以通过-v参数来挂载一个数据卷到某个容器内目录，命令格式如下：

docker run --name ng -v html:/root/html -p 8080;80 -d nginx

这里的-v就是挂载数据卷的命令：

- -v html:/root/html：把html数据卷挂在到容器内的/root/html目录中

给mysql挂载本地目录

容器不仅仅可以挂载数据卷，也可以直接挂载到宿主机目录上。关系如下：

- 带数据卷模式：宿主机目录 ----> 数据卷-----> 容器内目录
- 直接挂在模式：宿主机目录----->容器内目录



语法：

目录挂载与数据卷挂载的语法是类似的：

- `-v[宿主机目录]:[容器内目录]`
- `-v[宿主机文件]:[容器内文件]`

需求：创建一个Mysql容器，将宿主机目录直接挂载到容器

### 步骤

1.`pull  / load`建立镜像

2.创建目录`/usr/mysql/data`

3.创建目录`/usr/mysql/conf`

4.将提供的`hmy.cnf`文件上传到`/usr/mysql/conf`

5.挂载`/usr/mysql/data`到mysql容器内数据存储目录

6.挂载`/usr/mysql/conf/hmy.conf`到mysql容器的配置文件

7.设置Mysql密码



### 实现过程

1.加载镜像

```shell
docker load -i mysql.tar
```

2.运行mysql容器

```shell
docker run \
--name mysql \
-e MYSQL_ROOT_PASSWORD=root \
-p 3309:3306 \
-v /usr/mysql/conf/hmy.cnf:/etc/mysql/conf.d/hmy.cnf \
-v /usr/mysql/data:/var/lib/mysql \
-d \
mysql:8.0.29
```

## 小结

`docker run`的命令中通过 -v 参数挂载文件或目录到容器中：

- `-v volume名称`：容器内目录
- `-v 宿主机文件`：容器内文件
- `-v 宿主机目录`：容器内目录

### 数据卷挂载与目录直接挂载的关系

- 数据卷挂载耦合度低，由docker来管理目录，但是目录较深，不好找
- 目录挂载耦合度高，需要我们自己管理目录，不过目录容易寻找查看

# 4.其他

#### docker 进入mysql容器

```
docker exec -it mysql bash
```