# TypeScript是什么？

- 以JavaScript为基础构建的语言
- 一个JavaScript的超集
- TypeScript扩展了JavaScript，并添加了类型
- 可以在任何支持JavaScript的平台中执行

注意：TS不能被JS解析器直接执行

```
graph LR
A-- 文字描述 --->B
或
A--> |文字| B
```

TS  ---- 编译 ---> JS

## TypeScript增加了什么？

类型

支持ES的新特性

添加ES不具备的新特性

丰富的配置选项

强大的开发工具

## TypeScript开发环境搭建

下载Node.js

安装Node.js

- 查看node版本 `node -v`

使用npm命令全局安装typescript

- `npm i -g typescript`

创建一个ts文件

使用tsc xxx.ts

- 进入命令行
- 进入ts文件所在目录
- 执行命令： `tsc xxx.ts`

### 第一个helloTS.ts

```typescript
console("Hello TypeScript!")
```

## 基本类型

### 类型声明

- 类型声明是TS非常重要的一个特点
- 通过类型声明可以指定TS中变量（参数、形参）的类型
- 指定类型后，当为变量赋值时，TS编译器会自动检查值是否符合类型声明，符合则赋值，否则报错
- 简而言之，类型声明给变量设置了类型，使得变量只能存储某种类型的值

#### 语法

```typescript
let 变量 类型:
let 变量 类型 = 值；
function fn(参数: 类型, 参数: 类型): 类型{
    ...
}
```

#### 类型

| 类型    | 例子                  | 描述                           |
| ------- | --------------------- | ------------------------------ |
| number  | 1，-33，2.5           | 任意数字                       |
| string  | "hi", "hello", "张三" | 任意字符串                     |
| boolean | true，false           | 布尔值                         |
| enum    | enum{A, B}            | 枚举，TS中新增类型             |
| array   | [1, 2, 3]             | 任意JS数组                     |
| 字面量  | 其本身                | 限制变量的值就是该字面量的值   |
| any     | *                     | 任意类型                       |
| unknown | *                     | 类型安全的any                  |
| void    | 空值（undefined）     | 没有值（undefined）            |
| never   | 没有值                | 不能是任何值                   |
| object  | {name: '麻小神'}      | 任意的JS对象                   |
| tuple   | [4, 5]                | 元素，TS新增类型，固定长度数组 |

##### number、string、boolean

##### : number 表示函数有返回值的类型

```typescript
// 声明一个变量a，同时指定他的类型为number
let a: number;

// a 的类型设置为了number，在以后的使用过程中 a 的值只能为number
a = 10;
a = 33;
// a = 'hello' //此行代码会报错，因为 a 的类型为 number ，不能赋值字符串

let b: string;

// 声明完变量直接进行赋值
let c:boolean = true;

// 如果变量的声明和赋值是同时进行的，TS可以自动对变量进行类型检测
let c = false;
c = true;

// JS中的函数是不考虑参数的类型和个数的
// function sum(a, b){
// 	return a + b;
// }
// console.log(sum(123, 456));//579
// console.log(sum(123, "456"));//"123456"

function sum(a: number, b: number): number{
  return a + b;
}
console.log(sum(123,456));
// 括号后的 : number 表示返回值的类型

let result = sum(123, 456);

// TS代码的明确、可控、好处
```

##### 联合类型、any、unknown

##### void、断言

```typescript
// 也可以直接使用字面量进行类型声明

let a:10；
a = 10;

// 可以使用 | 来连接多个类型（联合类型）
let b: "male" | "female";
b = "male";
b = "female";

let c: boolean | string;
c = true;
c = 'hello';

// any表示任意类型，一个变量设置为any相当于对该变量关闭了TS的类型检测
// 使用TS时，不建议使用 any 类型
// let d: any;

//声明变量如果不指定类型，则TS解析器会自动判断变量的类型为any
let d;
d = 10;
d = 'hello';
d = true;

// unknown 表示未知类型的值
e = 10;
e = "hello";
e = true;

// unknown 实际上就是一个类型安全的any
// unknown 类型的变量，不能直接赋值给其他变量
if(typeof e === "string"){
	s = e;
}

//类型断言，告诉编译器变量的实际类型
/*
*语法：
*变量 as 类型
*<类型>变量
*/
s = e as string;
s = <string>e;

// void用来表示空，以函数为例，就表示没有返回值的函数
function fn(): void{
  return null;
}

// never 表示永远不会返回结果
function fn2(): never{
  throw new Error('报错了！');
}
```

##### object、函数的类型声明、数组

##### 元组、枚举、类型的别名

```typescript
// object表示一个js对象
let a: object;
a = {}
a = function (){};

// {}用来指定对象中可以包含那些属性
// 语法：{属性名:属性值， 属性名:属性值}
// 在属性名后边加上 ? 表示属性是可选的
let b: {name: string, age?: number};
b = {name: 'mason',age: 18};

//  [propName: string]: any 表示任意类型的属性
let c: {name: string, [propName: string]: any};
c = {name: 'mason',age: '20',gender: '男'};

/*
*设置函数的类型声明
* 		语法：(形参:类型， 形参:类型 ...) =>返回值
*/
let d: (a: number, b: number) => number;
d = function(n1, n2): number{
  return n1+n2;
}

/*
*		数组的类型声明：
*				类型[]
*				Array<type>
*/
// string[] 表示字符串数组
let e: string[];
e = ['a', 'b', 'c'];

// number[] 表示数值数组
let f: number[];

let g: Array<number>;

/*
*    元组，元组就是固定长度的数组
*        元组[类型，类型，类型]
*/
let h: [string, string];
h = ['hello', 'abc'];

/*
*		enum 枚举
*		
*/
enum Gender{
  Male = 0,
  Female = 1,
}

let i:{name: string, gender: Gender};
i = {
  name: 'mason',
  gender: Gender.Male;
}
console.log(i.gender === Gender.Male);

let j: {name: string} & { age: number};

//类型的别名
type myType = 1 | 2 | 3 | 4 | 5;
let k: myType;
let l: myType；
let m: myType;
```

# 配置选项

## 1. include

定义希望被编译文件所在目录

默认值：`["**/*"]`

示例：

```json
"include":[
  "src/**/*","tests/**/*"
]
```

以上例子中，所有src目录和test目录下的文件都会被编译

### a. exclude

定义需要排除在外的目录

默认值：`["node_modules","bower_components","jspm_packages"]`

示例：

```json
"exclude":[
  "src/hello/**/*"
]
```

以上示例中，src下hello目录下的文件都不会被编译

### b. extends

定义被继承的配置文件

示例：`"extends" : "./config/base"`

以上示例中，当前配置文件会自动包含config目录下base.json中的所有配置信息

### c. files

指定被编译文件的列表，只有需要编译的文件少时才会用到

示例：

```json
"files" :[
  "cores.ts",
  "sys.ts",
  "types.ts",
  "scanner.ts",
  "parser.ts",
  "binder.ts",
  "tsc.ts"
]
```

列表中的文件都会被TS编译器所编译

### d. compilerOptions

编译选项是配置文件中非常重要也比较复杂的配置选项

在compilerOptions中包含了多个子选项，用来完成对编译的配置

#### 项目选项

#### ⅰ. target

- 设置ts代码编译的目标版本
- 可选值

ES3（默认）、ES5、ES6/ES2015、ES7/ES2016、ES2017、ES2018、ES2019、ES2020、ESNext

- 示例

```json
"compilerOptions:{
	"target": "ES6"
}"
```

如上，我们编写的代码会被编译为ES6版本的js代码

#### ⅱ. module

- module 指定我们要使用的模块化规范
- 可选值 ’none‘， ’commonjs‘, 'amd', 'system', 'es6', 'es2015', 'es2020', 'esnext'
- 示例`"module": "system",`

#### ⅲ. lib

- 指定代码运行时所包含的库（宿主环境）
- 可选值

ES5、ES6/ES2015、ES7/ES2016、ES2017、ES2018、ES2019、ES2020、EsNext、DOM、WebWorker、SCriptHost

- 示例

```json
"lib": ["dom"]
```

#### ⅳ. outDir

- 用来指定编译后文件所在目录
- 示例`"outDir": "./dist",`

#### ⅴ. outFile

- 将我们的代码合并为一个文件
- 设置outFile后，所有的全局作用域中的代码会合并到同一个文件中
- 有点不灵活，结合打包工具使用
- 示例 `"outFile": "./dist/app.js"`

#### ⅵ. allowJS

- 是否对js文件进行编译，默认时false
- 示例`"allowJS: true"`

#### ⅶ. chechJS

- 是否检查js代码是否符合语法规范
- 示例`"checkJS": true`

#### ⅷ. removeComments

- 是否移除注释
- 示例 `"removeComments": true`

#### ⅸ. noEmit

- 不生成编译后的文件
- 示例 `"noEmit": true`

#### X. noEmitOnError

- 当有错误时，不生成编译后的文件
- 编译更严格时，避免出现错误代码
- 示例 `"noEmitOnError": false`

#### Xⅰ. strict

- 所有严格检查的总开关，true / false 全部打开
- 示例` "strict": false`

#### Xⅱ. alwaysStrict

- 是否开启严格模式
- 示例 `"alwaysStrict": true`

#### Xⅲ. noImplicitAny

- 不允许隐式的any类型
- 示例 `"noImplicitAny": false`

#### Xⅳ. noImplicitThis

- 不允许不明确类型的this，确定知道this的类型
- 示例 `"noImplicitThis": true`

#### Xⅴ. strictNullChecks

- 严格检查空值
- 示例 `"strictNullChecks": true`

# Webpack打包ts（一）

## 1. 初始化项目

* 使用命令`npm init -y`生成package.json

* 使用`cnpm i -D webpack webpack-cli typescript  ts-loader`下载依赖

* cnpm的下载 `npm install -g cnpm --registry=https://registry.npm.taobao.org`

## 2. 编写 webpack.config.js文件

```javascript
// 引入一个包
const path = require('path');

// webpack中所有的配置信息都应该卸载module.exports中
module.exports = {

    // 指定入口文件
    entry: './src/index.ts',

    // 指定打包文件所在的目录
    output: {
        //指令打包文件的目录
        path: path.resolve(__dirname, 'dist'),
        // 打包后文件的名字
        filename: "bundle.js"
    },

    // 指定webpack打包时要使用的模块
    module: {
        // 指定要加载的规则
        rules: [
            {
                // test指定规则生效的文件
                test: /\.ts$/,
                // 要使用的loader
                use: 'ts-loader',
                // 要排除的文件
                exclude: /node_modules/
            }
        ]
    }
};
```

## 3. tsconfig.json文件

```json
{
  "compilerOptions": {
    "module": "ES2015",
    "target": "ES2015",
    "strict": true
  },
}
```

## 4. 修改package.json

* 加入命令 `“build”: "webpack -mode development"`

## 5. 打包项目

* 终端运行 `npm run build`

# webpack打包ts（二）

## 1.帮助自动生成html

* 终端输入命令 `cnpm i -D html-webpack-plugin`



## 2.webpack开发服务器

* 终端输入命令`cnpm i -D webpack-dev-server`

* 在项目中安装内置服务器，与webpack关联，根据项目自动刷新

* 在package.json中加入`"start": "webpack serve --open --mode development"`



## 3.插件清空dist文件夹，重新放入打包生成文件

* 终端输入命令 `cnpm i -D clean-webpack-plugin`



```javascript
// 引入一个包
const path = require('path');

const HTMLWebpackPlugin = require('html-webpack-plugin');
//引入clean插件
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
// webpack中所有的配置信息都应该卸载module.exports中
module.exports = {

  // 指定入口文件
  entry: './src/index.ts',

  // 指定打包文件所在的目录
  output: {
    //指令打包文件的目录
    path: path.resolve(__dirname, 'dist'),
    // 打包后文件的名字
    filename: "bundle.js"
  },

  // 指定webpack打包时要使用的模块
  module: {
    // 指定要加载的规则
    rules: [
      {
        // test指定规则生效的文件
        test: /\.ts$/,
        // 要使用的loader
        use: 'ts-loader',
        // 要排除的文件
        exclude: /node_modules/
      }
    ]
  },
  //配置webpack插件
  plugins: [
    new CleanWebpackPlugin(),
    new HTMLWebpackPlugin({
      title: "这是一个自定义title",
      template: "./src/index.html"
    }),
  ],
  //用来设置引用模块
  resolve: {
    extensions: ['.ts','.js']
  }
};
{
  "compilerOptions": {
    "module": "ES2015",
    "target": "ES2015",
    "strict": true
  },
}
```