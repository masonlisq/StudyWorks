# MYSQL

### 什么是mysql

数据库：存储数据的仓库，数据是有组织的进行存储

数据库管理系统：操纵和管理数据库的大型软件

SQL：操作关系型数据库的编程语言，定义了一套操作关系型数据库统一标准。



在Web应用中，使用的最多的就是MySQL数据库，原因如下：

- 开源、免费
- 功能足够强大，足以应付web应用开发

关系型数据库都是使用SQL语言来进行同意操作，SQL语言是操作关系型数据库的统一标准。

### mysql安装与启动

- 安装成功后登录数据库（默认运行端口是3306）

`mysql -u root -p`MYSQL内置一个用户账号为root，密码为空

`select version();` #查询MYSQL版本

解决root无密码登录不了的问题

已不检查权限的方式启动，先停止了mysql服务

```sql
safe_MySQLd --skip -grant -tables
update MySQL.user set password
=PASSWORD('新密码') where User = 'root'
flush privileges;
```

解决端口冲突问题

```shell
netstat -ano|findstr 3306
```

查询正在执行3306端口的程序，如果有，在任务栏找到程序任务并结束

退出数据库，`exit` / `quit` 都可以退出数据库

```shell
exit` `quit
```

### 登录数据库

```shell
mysql [-h 127.0.0.1] [-P 3306] -u root -p
```

-h：主机IP

-P：服务端口，默认3306

-u：数据库用户名

-p：用户密码

### 将mysql添加到服务

以管理员的权限启动`cmd`，使用命令进入到`[mysql\bin]`

执行如下命令：

mysqld --install(服务名)

eg:mysqld --install Mysql

删除服务：

mysqld --remove 服务名

### 添加环境变量

将mysql的bin目录地址添加到系统环境变量 -- PATH中



### 关系型数据库

概念：建立在关系模型上，由多张相互连接的二维表组成的数据库

- 使用表存储数据，格式统一，便于维护
- 使用SQL语言操作，标准统一，使用方便

### 数据模型

客户端连接数据库管理系统，数据库管理系统中有多个数据库，数据库中有多个表

### SQL通用语法

- 单行或者多行书写，以分号结束
- 可以使用空格或者缩进增强语句可读性
- mysql数据库中，SQL语句不区分大小写，关键字建议大写
- 注释：单行 `#`` --`  多行 `/**/`

### 分类

DDL：数据定义语言，用来定义数据库对象（数据库，表，字段）。

DML：数据操作语言，用来对数据表中的数据进行增删改。

DQL：数据查询语言，用来查询数据库中表的记录。

DCL：数据控制语言，用来创建数据库用户，空值访问数据库的访问权限。



### DDL语句

#### 数据库

```sql
# 查看数据库
show databases;

# 创建新数据库
CREATE DATABASE [IF NOT EXISTS] 数据库名 [DEFAULT CHARSET utf8mb4];

# 选择数据库
use 数据库名;

select database();

# 删除数据库
drop database 数据库名;
```

#### DDL -- 表管理

```sql
# 查看当前被选中的数据库中的所有表
show tables;

# 创建表
CREATE TABLE [IF NOT EXISTS] tab_name(
  col_name datatyle [COMMIT '注释'],
  col_name datatype
)[CHARACTER set 编码格式];

# 查看表结构
describe 表名;
desc 表名;
show create table 表名;

# 删除表
drop table 表名;
```

#### 用户管理

```sql
# 查看当前登录的用户
select user() [from dual];

# 创建新用户
create user 用户名@'ip主机地址192.168.31.34' identified by '密码';

# 修改密码
alter user 用户名@'ip主机地址' identified by '密码' password expire never;
# 修改密码后不需要重新登录

# 查询用户信息
select user,host from mysql.user;

# 用新用户登录
mysql -u 用户名 [-h ip地址] -p
# 新创建的用户只有登录权限，需要使用管理员帐号授权

# 为用户授权
show grants for 用户名@localhost
# 查询用户的权限

grant all on kfm.carts to txsy@localhost;
# all代表所有权限

# 授权的新用户需要重新登陆才能使用新权限

# 任意ip可以访问
GRANT ALL PRIVILEGES ON *.* TO '用户名'@'%';
update mysql.user set host='%' where user='txsy';

flush privileges;
```

`all`：所有权限

`select`：查询权限

`insert`：插入权限

`update`：更新权限

删除用户

```
drop user 用户名@'localhost';
```

#### 创建表

常用数据类型：数值类型、字符串类型、日期时间类型。

| 数据类型xiaos  | 大小          | 描述                                                         |
| -------------- | ------------- | ------------------------------------------------------------ |
| tinyInt        | 1 byte        | 小整数值，带符号-128~127                                     |
| smallInt       | 2 bytes       | 小的整数，带符号的范围是-32768到32767，无符号范围为0-65535   |
| mediumInt      | 3 bytes       | 中等大小整数                                                 |
| int/Integer    | 4 bytes       | 普通大小整数，-2147483648到2147483647，0到16777215           |
| bigInt         | 8 bytes       | 大整数                                                       |
| float[(M,D)]   | 4 bytes       | 小（单精度）浮点数，实际范围是根据硬件或操作系统的不同可能稍微小些，M表示所有位数，D表示小数位数 |
| double[(M,D)]  | 8 bytes       | 普通大小（双精度）浮点数                                     |
| decimal[(M,D)] |               | 存储任何具有M位数字和D位小数的值                             |
| date           |               | 日期，mysql以'yyyy-mm-dd‘格式显示data值，允许字符串（‘20230721’/‘2023-07-21’）为date列分配值 |
| dateTime       |               | 日期时间的组合，YYYY-MM-DD HH:MM:SS                          |
| TimeStamp      |               | 时间戳                                                       |
| Time           |               | 时间                                                         |
| year           |               | 两位或四位格式的年，默认四位格式。四位：1901-2155和0000，两位：70-00和00-69 |
| char(M)        | 0-255 bytes   | 固定字符串，M表示列长度，M的范围是0-255                      |
| varchar(M)     | 0-65535 bytes | 变长字符串，M表示最大列长度，M的范围是0-65535                |
| tinyBlob       | 0-255 bytes   | 不超过255个字符的二进制数据                                  |
| tinyText       | 0-255 bytes   | 短文本字符串                                                 |
| Blob[(M)]      |               | 最大长度为65535(2^16-1)字节的BLOB列                          |
| text[(M)]      |               | 长字符串                                                     |
| mediumblob     |               | 二进制形式的中等长度文本数据                                 |
| mediumtext     |               | 中等长度文本数据                                             |
| longLob        |               | 二进制形式的极大文本数据                                     |
| LongText       |               | 极大文本数据                                                 |

char 与varchar 都可以描述字符串，插入、是定长字符串，指定长度多长就占用多少个字符，和字段值的长度无关。而varchar是变长字符串，指定长度为最大占用长度。

创建表

```sql
create table [if not exists] tab_name(
  col_name datatype [commit '注释'],
  col_name datatype
);

# 创建一个学生表：学生编号、学生姓名、出生日期
create table if not exists student (
  id int comment '编号',
	name varchar(50) comment '姓名',
  birth date comment '出生日期'
);

# 部门表
create table if not exists dept(
  dept_no int commtent '部门编号',
  d_name varchar(14) '部门名称',
  loc varchar(13) '部门地址'
  );

# emp表
create table if not exists emp(
  emp_no int comment '员工编号',
  e_name varchar(10) comment'员工姓名',
  job varchar(10) comment '职位',
  mgr int comment '员工上级领导编号',
  hirdate dateTime comment '入职时间',
  sal double comment '工资',
  comm double comment '奖金',
  dept_no int comment '所属部门编号'
);
```

#### 表操作-修改（DDL）

添加列

```sql
alter table 表名 add [column] 字段名 类型;
```

修改列

```sql
alter table 表名 modify [column] 字段名 类型;
```

修改列名称和类型

```sql
alter table 表名 change [column] 原字段名 新字段名 新列类型 [comment 注释] [约束]
```

删除列

```sql
alter table 表名 drop [column] 字段名;
```

修改表名

```sql
rename table 旧名称 to 新名称;
-- 或者
alter table 表名称 rename to 新名称;
```

删除表

```sql
drop table [if exists] 表名;
```

截断表

`truncate table 表名; `# 删除了会创建一个空表

删除表的时候，表中的全部数据也会被删除

创建和某表一样的表

```sql
create table 表名 like 要复制的表;

# 当要复制的表不在当前数据库时
create table 表名 like 数据库名，要复制的表;
```

### 数据管理

```sql
insert into 表名[(字段名称1，字段名称2，字段名称3)] 
value(value1,value2,value3);

insert into 表名[(字段名称1，字段名称2，字段名称3)]
values(value1,value2,value3);

# 字段需一一对应插入
insert into 表名 values (),(),();
select *(字段1，字段2，字段3) from 表名;

# 等值查询
select * from 表名 where 字段名 = value;

# 设置列别名
select 字段名 [as] '别名' from 表名 where 条件;

# 剔除重复行
select distinct * from 表名;
# 只是在显示的时候不显示重复的数据，数据未删除
update 表名 set 字段 = value where 条件;
# 修改满足条件的字段值
delete from 表名 where 条件;
# 删除满足条件的行
```

### 数据备份

```shell
mysqldump [选项] 数据库名 [表名] > 地址
```

参数说明：

| 参数名          | 缩写 | 含义                                                  |
| --------------- | ---- | ----------------------------------------------------- |
| --host          | -h   | 主机地址                                              |
| --port          | -P   | 服务器端口号                                          |
| --user          | -u   | MysqL用户名                                           |
| --password      | -p   | Mysql密码                                             |
| --databases     | -B   | 指定要备份的数据库                                    |
| --all-databases | -A   | 备份mysql服务器上的所有数据库                         |
| --no-data       | -d   | 不备份数据，默认为备份数据                            |
| --comments      | -i   | 是否有信息备注，默认为打开的，使用--skip-comments关闭 |

#### 备份表结构

```shell
mysqldump -u -root -p 数据库名 表1 表2 > 地址

# -d 只备份表结构
mysqldump -u root -p -d kfm01 dept > e:/kfm/dept/sql

#数据 + 表结构一起备份
mysqldump -u root -p kfm01 >e:/kfm/kfm01.sql

#
mysqldump -u root -p -d --skip-comments store cart > E:/cart.sql
```

#### 备份多个数据库

```shell
mysqldump -u root -p -d --databases 数据库1 数据库2 > 地址

mys1ldummp -u root -p -d -B kfm01 kfm02 > E:/kfm/test.sql

# --all-databases 备份所有数据库
mysqldump -u root -p -d --all-databases | -A > E:/kfm/allbackupfile.sql
```

#### 备份数据和结构

```shell
# 备份命令去掉 -d
mysqldump -u root -p --databases 数据库1 数据库2 > 地址
```

#### 将查询的结果集保存为文件

```shell
mysql -u -root -p -e 'select * from 数据库.表名' > 地址

mysql -u root -p -e "select * from store.good" > E:/result.txt

mysql -u -root -p -e "select * from goods" store > E:/result.csv
```

还原以前的数据

```shell
# 登录选中数据库之后执行，将数据还原到该数据库
source 地址; # sql语句

在服务器外面使用 mysql 命令还原
mysql -u root -p 新数据库名 < 地址

mysql -u root -p kfm < E:/cart.sql
```

---

# 

# 约束

## 1. 基本概念

约束是做用于表中字段的规则，用于限制存储在表中的数据。

目的：保证数据库数据的正确、有效性和完整性。

约束是为了保证进入数据库的数据都是有效的、可靠的，会对列的值进行一些约束，确保存进去的数据都是有效的。

查看约束 `show create table table_name`;

### 分类

| 约束     | 描述                                                     | 关键字      |
| -------- | -------------------------------------------------------- | ----------- |
| 主键约束 | 主键是一行数据的唯一标识，要求非空且唯一                 | primary key |
| 唯一约束 | 保证该字段的所有数据都是唯一，不重复的                   | unique      |
| 默认约束 | 保存数据时，如果未指定该字段的值，则采取默认值           | default     |
| 非空约束 | 限制该字段的数据不能为null                               | not null    |
| 检查约束 | 保证字段值满足某一个条件                                 | check       |
| 外键约束 | 用来让两张表的数据之间建立连接，保证数据的一致性和完整性 | foreign key |

约束是作用于表中字段上的，可以在创建表 / 修改表的时候添加约束。

## 2. 主键约束（PK）

主键约束的最显著的特征是主键列中的值不允许重复的，通过主键约束可强制表中的实体完整性。当创建或更改表时可通过定义primary key约束来创建主键。一个表只能有一个primary key约束，且 primary key约束中的列不能接受 NULL值。

```sql
alter table tab_name add constraint pk_name primary key (字段名);
-- 设置该字段为主键，主键约束名称为pk_name
```

### a. 设置主键的几种方式

```sql
-- 1.创建表的时候指定主键约束
create table `table_name` (
	`id` int primary_key,--设置主键
   `name` varchar(20),
   );
create table `table_name` (
  `id` int,
  `name` varchar(20),
  primary key (`id`)
);
create table `table_name`(
  `id` int,
  `name` varchar(20),
  constraint pk primary key(id) --设置主键
);

-- 2.修改某一列为主键
alter table 表名 add [constraint] primary key(id);
alter table 表名 modify [column] 字段名 属性 primary key; --修改列类型
alter table 表名 change [column] 字段名 字段名 属性 primary key;--修改列名和类型
```

删除主键约束

```sql
alter table 表名 drop primary key; --mysql 8.0.22之后的版本
alter table 表名 drop index primary key; --mysql 8.0.22 之前的版本
```

### b. 自增长列

并不是所有表在设计完成后都能找到适合作为主键的列，为此数据库提供了自增长列，自增长列是int类型的，其值是由数据库自动维护的，是永远不会重复的，因此自增长列是最适合作为主键列的。在创建表时，通过auto_increment关键字来标识自增长列，再Mysql数据库中自增长列必须是主键列。

特点：

- 标识列必须和一个key搭配（key指主键、唯一、外键）
- 一个表最多有一个标识列
- 标识列的；类型只能是数值型
- 标识列可以通过set auto_increment_increment  = 3;设置步长全局（退出数据库重新进入会恢复默认值），可以通过插入行时手动插入标识列值设置起始值。

```sql
create table goods(
  no int primary key auto_increment, --设置自增长
  name varchar(10)
);

alter table 表名 modify [column] 列名 列类型 auto_increment; --修改为自增长列
alter table 表名 change 列名 列名 列类型 auto_increment;
--删除自增长列
alter table 表名 modify [column] id int;
```

### c. 联合主键

联合主键是指在数据库表中，由多列共同组成的主键，用来唯一标识表中的每一行数据。它的作用类似于单一列的主键，但不是由单个列组成，而是由多个列组合而成。联合主键可以确保表中的每一行都具有唯一性，并且每个列组合的值都不会重复。

在创建表时，可以在列定义中指定多个列作为联合主键。

```sql
create table 表名(
  列名1 数据类型,
  列名2 数据类型,
  列名3 数据类型,
  primary key(列名1，列名2，列名3)
);

-- 修改列时创建
alter table 表名 add [constraint] primary key (列名1，列名2，列名3);

--删除
alter table 表名 drop primary key;
```

#### 联合主键在以下情况非常有用：

- 当单个列无法标识表中的每一行，但多个列组合在一起可以唯一标识每一行数据时。
- 提高查询性能：联合主键可以更有效的支持涉及多个列的查询，避免创建额外的索引。
- 在具有多个外键的关键表中，可以使用联合主键来确保外键引用的准确性。

联合主键要求每个列的组合都是唯一的。

## 3. 唯一约束

对于非主键列中的值也要要求唯一性时，就需要唯一约束

```sql
-- 创建表时
create table `table_name`(
  `id` int,
  `name` varchar(20) unique  --唯一约束
);

create table `table_name`(
  `id` int,
  `name` varchar(20),
  constraint up unique(name)  --唯一约束
);
--修改表
alter table 表名 add unique(列名称);
alter table 表名 add constraint [constraint_name] unique(列名称);
alter table 表名 change[column] 列名 列名 类型 unique;
alter table 表名 modify[column] 列名 列类型 unique;

--删除唯一约束
alter table 表名称 drop index 设置唯一时的名称;
--如果没有设置约束名称，名称默认是字段名
```

唯一约束允许有多个null值

## 4. 默认约束

为列中的值设置默认值，default value

```sql
--创建表时
create table 	`table_name`(
  `id` int default value;
  `name` varchar(20) value;
);
--修改表
alter table 表名 modify [column] 列名 列类型 default 默认值;
alter table 表名 change 列名 列名 列类型 default 默认值;

--删除
alter table 表名 modify [column] 列名 列类型;
alter table 表名 alter column 列名 drop default;--[8.0.23以上的版本]
alter table 表名 alter column 列名 set default null;[8.0.23以前的版本]
```

如果已经设置了值，默认值就无效了

## 5. 非空约束

not null:非空，用于保证该字段的值不能为空

```sql
create table `table_name`(
  `id` int not null, --非空约束
  `name` varchar(20)
);
alter table 表名 modify [column] 列名 列类型 not null;
alter table 表名 change 列名 列名 列类型 not null;

--约束删除
alter table 表名 modify [column] 列名 列类型 [null];
```

**修改列的约束确保现有数据满足非空条件，否则可能导致操作失败。**

# 

---

# 条件查询[扩展]

## 条件过滤

```sql
select * from dept;

select * from dept where dept_no = 3;

select * from dept where dept_no between 1 and 3;

select * from dept where dept_no in (1,2,3);

select * from dept where dept_no not in (2,4,6);


select * from 表名 where 字段 = value；
-- where子句表示查询的条件
-- [value1,value3]

select * from 表名 where 字段 between value1 and value2;

-- 字段在[value1,value2 ... ]任意一个就可
select * from 表名 where 字段 in (value1,value2,value3);

-- 字段不在[value1,value2,value3]
select * from 表名 where 字段 not in (value1,value2,value3);

-- & 条件1 和条件2 都为true 的结果提示
select * from 表名 where 条件1 or 条件2;
```

## 算数运算

```sql
--可以对查询出来的结果进行算术运算
select 字段1 + 字段2 from 表名;

--修改的时候也可以进行算术运算
update 表名 set 字段 = 字段 +value where 条件;
```

## NULL值查询

```sql
--NULL值无法通过等值操作查询
--NULL值指的是未填值，注意跟空字符串做区分
select * from 表名 where 字段 is null;
select * from 表名 where 字段 is not null;
```

## 模糊查询

```sql
--like 模糊查询 _ 表示一个任意字符 %表示零个或多个任意字符

select * from 表名 where 字段 like 'x_';

select * from 表名 where 字段 like 'x%';

select * from 表名 where 字段 like '%x%';

select * from 表名 where 字段 like '_x%';
```

## 逻辑运算

```sql
select * from 表名 where 字段 > value;
--where 条件语句里面可以写 > < = != 等
```

## 排序

```sql
--order by 对结果集排序，desc降序，asc升序（默认）
select * from 表名 order by 字段1 desc,字段2 asc;

--先按照dept_no降序排序，如果dept_no相等，按照d_name降序排序
select * from dept order by dept_no desc,d_name desc;
select * from 表名 order by 字段 asc;
```

## 分页查询

```sql
--limit 对结果集分页 参数1：起始行 参数2：显示条数
--页码：n 页大小 s limit(n-1)*s,s
select * from 表名 limit 0,2;
--从第0行开始显示两条数据
```

## 单行函数

```sql
--length 计算长度
select length("123");
select length(字段) from 表;

--upper/lower 大、小写转换
select upper("a"),lower("A");
select upper(字段),lower(字段) from 表;

--concat 字符串拼接
select concat(upper('smith'),'john');
select concat(字段1，字段2)from 表;
```

## 困难语句

扩展题目

统计1975年以后入职，部门人数超过2人的部门，按照部门人数从多到少排序输出， 分页显示，每页2条,显示第1页。

```sql
SELECT DEPTNO 部门, COUNT(1) 部门人数
FROM emp
WHERE HIREDATE > '19750101'
GROUP BY DEPTNO
HAVING COUNT(1) >= 2
ORDER BY COUNT(1) DESC
LIMIT 0,2
```

## select语句的书写顺序

完整语句：

```sql
select 查询对象
from 表名
where 条件
group by 分组列名
having 分组条件
order by 排序升降
limit 分页显示
```

## select语句的执行顺序

```sql
1.from 先搞清表的来源

2.where 描述查询条件

3.group by 然后进行分组

4.聚合 聚合函数计算 count / max / min /avg

5.having 对分组结果筛选

6.select 查询结果

7.distinct 对查询结果去重

8.order by 对查询结果排序

9.limit 分页显示第几页，显示几行数据
```

# 

# 多表查询[扩展]

## 1.火腿肠被谁买了？

```sql
SELECT 
	account.name,goods_name
FROM 
	goods,cart,account
WHERE 
	cart.goods_no = goods.good_no AND cart.account_id = account.id
	AND goods.goods_name='火腿肠'
```

![Snipaste_2023-07-25_21-12-50](https://gitee.com/masonlisq/mason_pic/raw/master/blogs_pic/202307252113374.png)

##  2.零食被谁买了？

```sql
SELECT 
	ac.name,go.goods_name,cg.name
FROM 
	account ac ,cart ca ,goods go ,category cg
WHERE
	cg.no = go.category_no 
	AND go.good_no = ca.goods_no
	AND ca.account_id = ac.id
	AND cg.name='零食'
```

![Snipaste_2023-07-25_21-21-49](https://gitee.com/masonlisq/mason_pic/raw/master/blogs_pic/202307252122819.png)

## 3.李四买了哪些商品？

```sql
SELECT goods_name,num,price,cart.create_time
FROM account,cart,goods
WHERE account.id=cart.account_id
	AND cart.goods_no = goods.good_no
	AND account.name='李四'
```

![Snipaste_2023-07-25_21-25-46](https://gitee.com/masonlisq/mason_pic/raw/master/blogs_pic/202307252126709.png)

## 4.所有用户分别买了多少钱？

```sql
SELECT account.name AS 客户,SUM((cart.num * goods.price )) 消费金额
FROM account,cart,goods
WHERE account.id = cart.account_id
	AND cart.goods_no = goods.good_no
GROUP BY account.name
```

![Snipaste_2023-07-25_21-34-08](https://gitee.com/masonlisq/mason_pic/raw/master/blogs_pic/202307252156147.png)



## 5.假设购物车东西全部销售，周几的营业额最高

```sql

```

![Snipaste_2023-07-25_21-55-05](https://gitee.com/masonlisq/mason_pic/raw/master/blogs_pic/202307252156303.png)

# 补充知识

```sql
# DAYOFWEEK ,DAYOFYEAR ,DAYOFMONTH
# select now() # 取日期和时间
# SELECT CURRENT_DATE #取日期
# SELECT CURRENT_TIME #取时间
# select DATE_FORMAT( NOW(),'%Y-%m-%e') # 提取 包含时间的日期 中的日期部分，不要时间部分
```

## DATE_FORMAT参数表

```
%a	缩写星期名
%b	缩写月名
%c	月，数值
%D	带有英文前缀的月中的天
%d	月的天，数值(00-31)
%e	月的天，数值(0-31)
%f	微秒
%H	小时 (00-23)
%h	小时 (01-12)
%I	小时 (01-12)
%i	分钟，数值(00-59)
%j	年的天 (001-366)
%k	小时 (0-23)
%l	小时 (1-12)
%M	月名
%m	月，数值(00-12)
%p	AM 或 PM
%r	时间，12-小时（hh:mm:ss AM 或 PM）
%S	秒(00-59)
%s	秒(00-59)
%T	时间, 24-小时 (hh:mm:ss)
%U	周 (00-53) 星期日是一周的第一天
%u	周 (00-53) 星期一是一周的第一天
%V	周 (01-53) 星期日是一周的第一天，与 %X 使用
%v	周 (01-53) 星期一是一周的第一天，与 %x 使用
%W	星期名
%w	周的天 （0=星期日, 6=星期六）
%X	年，其中的星期日是周的第一天，4 位，与 %V 使用
%x	年，其中的星期一是周的第一天，4 位，与 %v 使用
%Y	年，4 位
%y	年，2 位
```



## 6.张三在什么时候购物

```sql
SELECT account.name, cart.create_time
FROM account INNER JOIN cart
ON account.id=cart.account_id
WHERE account.name = '李四'
```



## 7.购物车里的商品销售后，赚了多少钱

```sql
SELECT  SUM((num*price)-(num * cost)) AS 利润
FROM cart INNER JOIN goods
ON cart.goods_no = goods.good_no
```



## 8.求哪个商品利润率最高

```sql
SELECT goods_name,CONCAT(((price - cost)/cost)*100,'%') AS 最高利润率 
FROM goods
ORDER BY (price - cost)/cost DESC 
LIMIT 0,1
```



## 9. 求2023年3月12日前一周上架的商品

```sql
SELECT * FROM goods 
WHERE DATE_FORMAT( create_time,'%Y-%m-%d') >= DATE_SUB('2023-03-12',INTERVAL 1 WEEK) 
	AND  DATE_FORMAT( create_time,'%Y-%m-%d')<='2023-03-12'

# SELECT DATE_FORMAT( create_time,'%y-%m-%e') FROM goods  
# date_sub() # 设置指定日期的时间间隔
# select date_sub(NOW(),interval 1 year)
```