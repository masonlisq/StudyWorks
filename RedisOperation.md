## （一）Redis软件安装


redis-server.exe		|服务器
-|-
redis-cli.exe|				命令行窗口客户端
RedisDesktopManager |图形化界面客户端

## （二）数据库的分类

SQL：关系型数据库。以表为逻辑结构来存储数据的。

NOSQL：非关系型数据库。

> 键值 ( Key-Value) 存储数据库
>
> 列存储数据库
>
> 文档型数据库
>
> 图形 (Grph) 数据库

通常：项目已关系型数据库为主、非关系型数据库为辅的开发模式

## （三）Redis的数据类型

（1）string字符串

> set name jack  //存 

> get name   //取

（2）list列表

>lpush color red		//存
>
>lpush color green
>
>lpush color blue

>lrange color 0 2		//取：起始区间（lrange listName start end）

（3）set集合

> sadd name amy
>
> sadd name jack
>
> sadd name sisi
>
> sadd name tom

>smembers name		//smembers setName

（4）zset有序集合

> zadd book 2 b
>
> zadd book 3 c
>
> zadd book 1 a

> zrangebyscore book 0 3

（5）map哈希

> hmset myhash k1 v1 k2 v2 k3 v3

> hget myhash k1
>
> hget myhash k2
>
> hget myhash k3

## （四）Redis在项目中缓存的具体应用

优化：
>
> 连接池技术：对数据库的连接进行优化。
>
> 缓存数据：对查询的数据进行优化。

##### 架构：

​							Java		ORM		Mysql

A		->

B		->			DAO层		->		部门表

C		->

效率低的原因：反复重复把表中的记录换行映射为Java中的对象

例如：100个请求、把表中的记录反复转换成100次对象、再返回

##### Redis架构：

​							Java		Redis（缓存）		Mysql（存储）

A		->

B		->			DAO层 ->	1、开发部		->		SQL

C		->

优点：每次访问数据、先访问缓存区中的数据，如果有则直接返回，如果没有，再访问Mysql中的数据、并存储到缓存区中。

#### （1）pom.xml配置redis依赖

```xml
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artfactId>spring-boot-starter-data-redis</artfactId>
</dependency>
```

#### （2）application.properties配置redis位置参数

```properties
#Redis
#Redis数据库索引（默认为0）
spring.redis.database=0
#Redis服务器地址（默认为127.0.0.1）
spring.redis.host=127.0.0.1
#Redis服务器端口（默认为6379）
spring.redis.port=6379
#Redis服务器连接密码（默认为空）
spring.redis.password=
#连接超时时间
spring.redis.timeout=2000
```

* ##### application.yml配置redis位置参数

```yml
spring:
　#redis配置
  redis:
 　　database: 0
  　　timeout: 0
　　　# Redis服务器地址
  　　host: 127.0.0.1
　　　# Redis服务器连接端口
  　　port: 6379
　　　# Redis服务器连接密码（默认为空）
  　　password: 
　　　# 连接池最大连接数（使用负值表示没有限制）
  　　pool:
  　　　max-active: 8
　　　　# 连接池最大阻塞等待时间（使用负值表示没有限制）
    　　max-wait: -1
　　　　# 连接池中的最大空闲连接
    　　max-idle: 8
　　　　# 连接池中的最小空闲连接
    　　min-idle: 0
```

#### （3）Redis配置类

>com.mason.config/RedisConfig

```java
/**
*Redis配置类
*
*/
@Configuration
public class RedisConfig {

    //编写我们自己的配置redisTemplate
    @Bean
    @SuppressWarnings("all")
    public RedisTemplate<String, Object> redisTemplate(RedisConnectionFactory redisConnectionFactory) {
        RedisTemplate<String, Object> template = new RedisTemplate<>();
        template.setConnectionFactory(redisConnectionFactory);

        // JSON序列化配置
        Jackson2JsonRedisSerializer jsonRedisSerializer=new Jackson2JsonRedisSerializer(Object.class);
        ObjectMapper objectMapper=new ObjectMapper();
        objectMapper.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY);
        objectMapper.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_FINAL);
        jsonRedisSerializer.setObjectMapper(objectMapper);

        // String的序列化
        StringRedisSerializer stringRedisSerializer=new StringRedisSerializer();

        //key和hash的key都采用String的序列化方式
        template.setKeySerializer(stringRedisSerializer);
        template.setHashKeySerializer(stringRedisSerializer);

        //value和hash的value都采用jackson的序列化方式
        template.setValueSerializer(jsonRedisSerializer);
        template.setHashValueSerializer(jsonRedisSerializer);

        template.afterPropertiesSet();

        return template;
    }
}

```

> 说明一：配置文件yml、配置类config
>
> 配置文件和配置类都会在服务器启动的时候被加载，共同的作用是用来配置项目信息
>
> 说明二：
>
> 配置文件主要配置静态信息
>
> 配置类配置动态信息

@Configuration注解	|		配置注解、作用：启动时加载本类、配置类
-|-
@EnableCaching注解	|		缓存注解、作用：开启缓存功能

#### （4）举例：部门数据添加到缓存中

注意：缓存的应用是选择的、不能都添加缓存。

> 序列化接口Serializable

```java
@Data
public class Department implements Serializable{
    private static final long serialVersionUID = -6993544107777788081L;
    
    private Integer id;
    private String name;
    private List<Person> list;
}
```

* ##### 序列化

  >  Seriaizable是IO流中的序列化接口

  序列化的概念：把Java对象流化。可以把Java对象以文件的形式存起来。

  Java对象										文件

  new Person("张三",30)  	-> 	person文件 

  new Person("张三",30)  	<- 	person文件 

  序列化：把Java对象存储到文件中。

  反序列化：把文件还原成Java对象

  > serialVersionUID			序列化ID、类的版本ID

#### （5）缓存注解

注解|缓存注解\|作用
-|-
@Cacheable(value="DepartmentCache")	| 查询操作 
@CachePut	| 添加、修改操作 
@CacheEvict	  | 删除操作 


