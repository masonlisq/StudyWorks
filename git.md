# git的常用命令

git clone <repo>	克隆仓库

git clone <repo><directory>	克隆到指定目录

$ git add <name>	跟踪一个文件或一个目录

$ git rm <name>	不被跟踪

$ git rm --cache <name>	保留在目录里不被跟踪

更新文件：

git add <file-name>	状态设置为缓存状态

git reset HEAD <name>	取消缓存状态

git commit	提交此次修改

#### 文件的四种状态

未跟踪|未修改|已修改|暂存

查看文件的状态

git status	查询文件状态

git diff	查询具体那里被修改

git log	查询历史提交

git log --pretty=oneline	每一次提交信息变为一行

$ git log --pretty=format:"%h - %an, %ar : %s"

###### 说明：%h 简化哈希	%an 作者名字	%ar修订日期（距今）%ad 修订日期	%s提交说明

git log --graph	图形化（分支）



#### 远程仓库

git remote add orgin <链接>

git remote	查看远程仓库连接

git rename orgin origin	更改名称

git push origin master	推送本地代码至远程仓库

分支

首次提交对象时新建  master  分支

#### 操作分支

查看此时处于何分支：

git log

git status

git branch --list

创建分支：

git branch feature1

切换分支：

git checkout feature1

新建并切换分支：

git checkout -b feature2

查看左右分支的提交：

git log --all

git log --all --graph

##### 合并分支：

git merge feature1

git merge feature2

git fetch	拉取远程分支

新建跟踪远程分支：

git checkout -b feature1 orgin/feature1

git checkout --track orgin/feature1

贮藏：

git stash

git statsh push

git statsh apply	恢复贮藏前的状态



重置：

git reset head~ --soft

变基（搬家）：

git rebase

git rebase -i



* git官方文档
* git 的GUI界面（VSCode、Idea）